<?php
declare(strict_types=1);

namespace App\Services;

class LogarigmCalculate implements CalculateInterface
{
    public function calculate($data)
    {
        $num = $data['num'] ? (int)$data['num'] : null;
        $base = $data['base'] ? (int)$data['base'] : null;

        return $num && $base ? log($num, $base) : null;
    }
}
