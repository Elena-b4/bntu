<?php
declare(strict_types=1);

namespace App\Services;

class Rad2DegCalculate implements CalculateInterface
{
    public function calculate($data)
    {
        $num = $data['num'] ? (int)$data['num'] : null;
        $precision = $data['precision'] ? (int)$data['precision'] : null;
        $result = $data['result'] ? (float)$data['result'] : null;

        return $precision || $num ? ($precision ? round($result, $precision) : rad2deg($num)) : null;
    }
}
