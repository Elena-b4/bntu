<?php
declare(strict_types=1);

namespace App\Services;

use App\Services\CalculateInterface;

class SqrtCalculate implements CalculateInterface
{
    public function calculate($data)
    {
        $num = (float)$data['num'];

        return $num ? sqrt($num) : null;
    }
}
