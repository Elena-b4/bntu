<?php

namespace App\Services;

interface CalculateInterface
{
    public function calculate($data);
}
