<?php
declare(strict_types=1);

namespace App\Services;

class PowCalculate implements CalculateInterface
{
    public function calculate($data)
    {
        $num = $data['num'] ? (int)$data['num'] : null;
        $exponent = $data['exponent'] ? (int)$data['exponent'] : null;

        return $num && $exponent && is_int($num) && is_int($exponent) ? pow($num, $exponent) : null;
    }
}
