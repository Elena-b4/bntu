<?php

namespace App\Http\Controllers;

use App\Services\CalculateInterface;
use App\Services\LogarigmCalculate;
use App\Services\PowCalculate;
use App\Services\Rad2DegCalculate;
use App\Services\SqrtCalculate;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class IndexController extends BaseController
{
    public function about()
    {
        return view('about');
    }

    public function logarifm()
    {
        return view('logarifm');
    }

    public function pow()
    {
        return view('pow');
    }

    public function sqrt()
    {
        return view('sqrt');
    }

    public function rad2deg()
    {
        return view('rad2deg');
    }

    public function main()
    {
        return view('main');
    }

    public function calculate(Request $request, $type)
    {
        try {
            switch ($type) {
                case 'logarifm':
                    app()->bind(CalculateInterface::class, LogarigmCalculate::class);
                    break;
                case 'pow':
                    app()->bind(CalculateInterface::class, PowCalculate::class);
                    break;
                case 'rad2deg':
                    app()->bind(CalculateInterface::class, Rad2DegCalculate::class);
                    break;
                case 'sqrt':
                    app()->bind(CalculateInterface::class, SqrtCalculate::class);
                    break;
            }
            $service = app()->make(CalculateInterface::class);

            return redirect()->back()->with(['result' => $service->calculate($request->all())]);
        } catch (\Exception $exception) {
            return redirect()->back();
        }
    }
}
