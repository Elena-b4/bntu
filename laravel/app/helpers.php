<?php
declare(strict_types=1);


if (!function_exists('compareWithPath')) {
    function compareWithPath($customPath): bool
    {
        return request()->path() === $customPath;
    }
}

