<?php

use App\Http\Controllers\IndexController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('/calculate/{type}', [IndexController::class, 'calculate'])->name('calculate');

Route::get('/',  [IndexController::class, 'about']);
Route::get('/logarifm',  [IndexController::class, 'logarifm']);
Route::get('/sqrt',  [IndexController::class, 'sqrt']);
Route::get('/rad2deg',  [IndexController::class, 'rad2deg']);
Route::get('/pow',  [IndexController::class, 'pow']);

Route::get('/main',  [IndexController::class, 'main']);
