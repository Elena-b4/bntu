@extends('base')
@include('nav')
@section('content')
<div class="container text-center">
    <form action="/calculate/{{ request()->path() }}" method="POST">
        @method('POST')
        @csrf
        <div class="row m-2 p-4">
            <div class="col-sm-4" style="display:block; text-align:left;">
                <div>Преобразовать значение <input size="4" name="num"> из радианов в градусы</div>
            </div>
            <div class="col-sm-8">
                <label> Результат:
                    <input name="result" value="{{ \Illuminate\Support\Facades\Session::get('result') }}">
                </label>
            </div>
        </div>
        <div class="row m-2 p-4">
            <div class="col-sm-12" style="display:block; text-align:left;">
                <label> Округлить результат до <input name="precision"> знаков после запятой</label>
            </div>
        </div>
        <div class="row m-3">
            <div class="col-sm-6">
                <button type="submit" class="btn btn-success">Вычислить</button>
            </div>
            <div class="col-sm-6">
                <button type="submit" class="btn btn-danger">Сбросить</button>
            </div>
        </div>
    </form>
</div>
@endsection
