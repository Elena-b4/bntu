@extends('base')
@include('nav')
@section('content')
<div class="container text-center">
    <div class="row m-2 p-4">
        <form action="/calculate/{{ request()->path() }}" method="POST">
            @method('POST')
            @csrf
            <div class="col-sm-4" style="display:block; text-align:left;">
                <div>Возведение числа <input size="4" name="num"> в степень <input size="4" name="exponent">

                </div>
            </div>
            <div class="col-sm-4 m-2 p-4" style="display:block; text-align:left;">
                Результат:
                <input name="result" value="{{ \Illuminate\Support\Facades\Session::get('result') }}">
            </div>
            <div class="row m-2 p-4">
                <div class="row m-3 p-4">
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-success">Вычислить</button>
                    </div>
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-danger">Сбросить</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
