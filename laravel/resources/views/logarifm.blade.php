@extends('base')
@include('nav')
@section('content')
<div class="container text-center">
    <form action="/calculate/{{ request()->path() }}" method="POST">
        @method('POST')
        @csrf
        <div class="row m-2 p-4">
            <div class="col-sm-4" style="display:block; text-align:left;">
                <div class="col-sm-9">
                    <label style="display:block; text-align:right;">
                        <input size="4" name="num">
                    </label>
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <p>Обазятельное поле</p>
                    </div>
                @endif
                <div>Вычислим логарифм log</div>
                <div class="col-sm-9">
                    <label style="display:block; text-align:right;">
                        <input size="4" name="base">
                    </label>
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <p>Обазятельное поле</p>
                    </div>
                @endif
            </div>
            <div class="col-sm-8" style="display:block; text-align:left;">
                <br>
                <div>
                    <label> Результат:
                        <input name="result" value="{{ \Illuminate\Support\Facades\Session::get('result') }}">
                    </label>
                </div>
            </div>
        </div>
        <div class="row m-3">
            <div class="col-sm-6">
                <button type="submit" class="btn btn-success">Вычислить</button>
            </div>
            <div class="col-sm-6">
                <button type="submit" class="btn btn-danger">Сбросить</button>
            </div>
        </div>
    </form>
</div>
@endsection

