<ul class="nav nav-tabs">
    <li class="nav-item">
        <a class="nav-link @if(compareWithPath('logarifm')) active @endif" href="/logarifm">Логарифм</a>
    </li>
    <li class="nav-item">
        <a class="nav-link @if(compareWithPath('pow')) active @endif" href="/pow">Возведение в степень</a>
    </li>
    <li class="nav-item">
        <a class="nav-link @if(compareWithPath('rad2deg')) active @endif" href="/rad2deg">Из радианов в
            градусы</a>
    </li>
    <li class="nav-item">
        <a class="nav-link @if(compareWithPath('sqrt')) active @endif" href="/sqrt">Квадратный корень</a>
    </li>
    <li class="nav-item">
        <a class="nav-link @if(compareWithPath('')) active @endif" href="/" style="color: #e4606d">На главную</a>
    </li>
</ul>
