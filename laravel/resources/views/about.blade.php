@extends('base')
@section('content')
    <header class="bg-primary bg-gradient text-white py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-3 text-left text-md-center mb-3">
                    <img class="rounded-circle img-fluid" src="{{ url('/storage/test/123.jpg') }}" alt="Profile Photo" />
                </div>
                <div class="col-md-9">
                    <h1>Елена Белоусова</h1>
                    <h5>Студентка группы 41702120</h5>
                    <hr>
                    <a href="/logarifm" target="_blank" ><button type="submit" class="btn btn-outline-light">Задания</button></a>
                    <a href="https://gitlab.com/Elena-b4/bntu" target="_blank" ><button type="submit" class="btn btn-outline-light">Репозиторий с исходным кодом</button></a>
                    <a download="Контрольная работа Белоусова Елена" href="{{ url('/storage/test/Разработка_приложений_в_визуальных_средах_ Белоусова.docx') }}"><button type="submit" class="btn btn-outline-light">Скачать контрольную работу</button></a>
                    <hr>
                    <div>
                        Cвяжитесь со мной:
                        <a href="https://t.me/B_Elena" target="_blank"><img src="{{ asset('/storage/test/800px-Telegram_Messenger.png') }}" width="4%"></a>
                        <a href="mailto:lena_helen_b@mail.ru" target="_blank"><img src="{{ asset('/storage/test/1280px-Mail.Ru_Logo_2018.svg.png') }}" width="10%"></a>
                    </div>
                </div>
            </div>
        </div>
    </header>
@endsection
